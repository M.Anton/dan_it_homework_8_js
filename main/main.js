let allParagraphs = document.querySelectorAll("p");
console.log(allParagraphs);
for (let paragraph of allParagraphs) {
    paragraph.style.backgroundColor = "#ff0000";
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

let parentElementOfOptionsList = optionsList.parentElement;
console.log(parentElementOfOptionsList);

let childNodesOfOptionsList = optionsList.childNodes;
for (node of childNodesOfOptionsList) {
    console.log(node, typeof(node));
    console.log(`${node} : ${typeof(node)}`);
}

let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

let mainHeader = document.querySelector(".main-header");

let mainHeaderChildren = mainHeader.children;

for (let child of mainHeaderChildren){
    child.classList.add("nav-item");
    console.log(child);
}

let sectionTitle = document.querySelectorAll(".section-title");
for (title of sectionTitle) {
    title.classList.remove("section-title");
    console.log(title);
}
